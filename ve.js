const CTEXT="полнодюплексный протокол с аппаратным квитированием пакетов"
const VE_KEY="антропоморфныйдендромутант"
var cText="";
var oText="";

var CaesarEncoder={
	alphabeth:"абвгдеёжзийклмнопрстуфхцчшщъыьэюя".split(''),
	shiftLength:4
	}
function cencode(strIn){
  var _self=this;
  return strIn.split('')
			.map(
				sym=>{
					let np=(_self.alphabeth.indexOf(sym.toLowerCase())+_self.shiftLength)%_self.alphabeth.length;
				   return _self.alphabeth[np];
				}).join('')
	}
function cdecode(strIn){
  var _self=this;
  return strIn.split('')
			.map(
				sym=>{
					let np=(_self.alphabeth.indexOf(sym.toLowerCase())+_self.alphabeth.length-_self.shiftLength)%_self.alphabeth.length;
				   return _self.alphabeth[np];
				}).join('')
	}
CaesarEncoder.encode=cencode.bind(CaesarEncoder);
CaesarEncoder.decode=cdecode.bind(CaesarEncoder);

cText=CaesarEncoder.encode(CTEXT);
oText=CaesarEncoder.decode(cText);
console.log(
`   Caesar test case:
                text2crypt=${CTEXT};    key=${CaesarEncoder.shiftLength};
                ________________________________________________________
                Encrypt([${CTEXT}],[${CaesarEncoder.shiftLength}]) -> [${cText}]
                Decrypt([${cText}]+[${CaesarEncoder.shiftLength}]) -> [${oText}]
                _________________________________________________________
    `
);

/******************************************************************************************/

class VigEncoder{
    constructor(k) {
        this.key=k.toUpperCase();
        this.alph_matrix={}
        this.#shuffle_alphabeth();
        return this;
    }
    static SHUFFLE_ROUNDS=100;
    static alphabeth="абвгдеёжзийклмнопрстуфхцчшщъыьэюя".toUpperCase().split('');

    static #shuffle(a,rounds=VigEncoder.SHUFFLE_ROUNDS){
        function swap_pair(alp,e1,e2){
            let buf=alp[e1];
            alp[e1]=alp[e2];
            alp[e2]=buf;
            return alp;
        }

        for(let i=0;i<rounds;i++){
            a=swap_pair(a,Math.floor((Math.random()*a.length)),~~(Math.random()*a.length))
        }

        return a;
    };

    #shuffle_alphabeth(){

        VigEncoder.alphabeth.map(symbol=>{
            let new_alp=VigEncoder.alphabeth.map(e=>e);
            this.alph_matrix[symbol]=VigEncoder.#shuffle(new_alp,VigEncoder.SHUFFLE_ROUNDS)
        })
    };

    encode(text){
        var counter=0;
        var dividor=this.key.length;
        var sliding_alp=undefined;

        var _self=this;
console.groupCollapsed('extra data')

        var x= text.toUpperCase().split('')
            .map(symbol=>{
                let i=VigEncoder.alphabeth.indexOf(symbol);
                return i<0?symbol:i;
            }).map(backIndex=>{
            if(!Number.isInteger(backIndex)){return backIndex}
            sliding_alp=_self.alph_matrix[_self.key[counter]];

console.log(`
${backIndex} - ${VigEncoder.alphabeth[backIndex]} 
${sliding_alp.join('_')}
${sliding_alp[backIndex]}
`)

            counter=(counter+1)%dividor;
            return sliding_alp[backIndex]
        }).join('')

console.groupEnd('extra data')

        return x

    };
    decode(encrypted){
        var counter=0;
        var dividor=this.key.length;
        var sliding_alp=undefined;

        var _self=this;
        return encrypted.toUpperCase().split('').map(
            symbol=>{
                sliding_alp=_self.alph_matrix[_self.key[counter]];
                let backIndex=sliding_alp.indexOf(symbol);
                if(backIndex<0){return symbol};
                counter=(counter+1)%dividor;
                return VigEncoder.alphabeth[backIndex];
            }).join('')
    }
}

ve=new VigEncoder(VE_KEY);
cText=ve.encode(CTEXT);
oText=ve.decode(cText);

console.log(`   VigEncoder test case:
                text2crypt=${CTEXT};    key=${VE_KEY};
                ________________________________________________________
                Encrypt([${CTEXT}],[${VE_KEY}]) -> [${cText}] 
                Decrypt([${cText}]+[${VE_KEY}]) -> [${oText}]
                _________________________________________________________
                `)

function counter(){
        let num=0;
	

        
	     
	    function incrementer(){
		     num++;
		     return num;
	    }
	    return incrementer;
	
}

var addF=counter()

console.log('HW');
console.log(addF());
